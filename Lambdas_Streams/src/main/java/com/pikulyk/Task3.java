package com.pikulyk;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task3 {

  private static List<Integer> list;
  private static Random random = new Random();

  public static List<Integer> getByStreamOfList() {
    return Stream.of(1, 10, 9, 3, 5, 2, 8, 4, 7, 6)
        .collect(Collectors.toList());
  }

  public static List<Integer> getByIterateList() {
    return Stream.iterate(0, n -> random.nextInt(10) + 1)
        .limit(10)
        .collect(Collectors.toList());
  }

  public static List<Integer> getByGenerateList() {
    return Stream.generate(() -> random.nextInt(10))
        .limit(10)
        .collect(Collectors.toList());
  }

  public double averageOfList(List<Integer> list1) {
    return list1.stream()
        .mapToInt(i -> i).average().getAsDouble();
  }

  public int minOfList(List<Integer> list1) {
    return list1.stream()
        .min(Comparator.comparingInt(Integer::intValue)).get();
  }

  public int maxOfList(List<Integer> list1) {
    return list1.stream()
        .max(Comparator.comparingInt(Integer::intValue)).get();
  }

  public int sumOfList(List<Integer> list1) {
    return list1.stream()
        .mapToInt(x -> x).sum();
  }

  public Stream<Integer> higherThanAvg(List<Integer> list1) {
    return list1.stream().filter(i -> i > averageOfList(list1)).distinct();
  }

  public int sumByReduce(List<Integer> list1) {
    return list1.stream()
        .reduce((a, b) -> b += a).get();
  }

  public static void main(String[] args) {
    Task3 obj = new Task3();
    list = Task3.getByStreamOfList();
    System.out.println(list);
    System.out.println("Average of numbers: " + obj.averageOfList(list));
    System.out.println("Min: " + obj.minOfList(list));
    System.out.println("Max: " + obj.maxOfList(list));
    List<Integer> higherThanAvg = obj.higherThanAvg(list).collect(Collectors.toList());
    System.out.println("Numbers higher than average: " + higherThanAvg);
  }
}
