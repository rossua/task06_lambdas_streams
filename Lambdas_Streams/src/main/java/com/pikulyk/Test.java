package com.pikulyk;

import com.pikulyk.task1.MyLambdaTask1;
import com.pikulyk.task2.CommandExecutor;
import java.util.Scanner;

public class Test {

  private static Scanner input = new Scanner(System.in);

  public static void testTask1() {
    //add test task1
  }

  public static void testTask2() {
    System.out.println("Command pattern:");

    CommandExecutor test = new CommandExecutor();
    String keyMenu;
    String arg;

    do {
      test.showMenu();

      System.out.print("Enter your choice: ");
      keyMenu = input.nextLine();

      if (keyMenu.equals("Q") || keyMenu.equals("q")) {
        System.out.println("Ending task 2");
        break;
      }

      if (!test.getCommand().containsKey(keyMenu)) {
        System.out.println("Wrong option."
            + "Please enter again!");
        continue;
      }

      System.out.print("Enter your string: ");
      arg = input.nextLine();

      test.getCommand().get(keyMenu).execute(arg);
    } while (true);
  }

  public static void testTask3() {
    //add test task3
  }

  public static void testTask4() {
    //add test task4
  }

  public static void main(String[] args) {
    MyLambdaTask1.main(args);
    Test.testTask2();
    Task3.main(args);
    Task4.main(args);
  }
}
