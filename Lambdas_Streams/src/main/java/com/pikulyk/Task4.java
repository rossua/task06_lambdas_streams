package com.pikulyk;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task4 {

  private List<String> userText;
  private Scanner scanner = new Scanner(System.in);

  public void start() {
    String line;
    userText = new ArrayList<String>();
    System.out.println("Enter your text (ending with empty line): ");
    while (true) {
      line = scanner.nextLine();
      if (line.isEmpty()) {
        break;
      }
      userText.add(line);
    }
  }

  public long numberOfUniqeWords() {
    return userText.stream().flatMap(e -> Stream.of(e.split(" "))).distinct().count();
  }

  public List<String> listOfUniqueWords() {
    return userText.stream().flatMap(e -> Stream.of(e.split(" ")))
        .distinct().sorted()
        .collect(Collectors.toList());
  }

  public void wordOccurence() {
    System.out.println("Word occurence: " + userText.stream()
        .flatMap(e -> Stream.of(e.split(" ")))
        .collect(Collectors.groupingBy(x -> x, Collectors.counting())));
  }

  public void symbolOccurence() {
    System.out.println("Symbol occurence: " + userText.stream()
        .flatMap(e -> e.chars().boxed()).map(x -> (char) x.intValue())
        .collect(Collectors.groupingBy(x -> x, Collectors.counting())));
  }

  public static void main(String[] args) {
    Task4 task4Obj = new Task4();
    task4Obj.start();
    System.out.println("Number of uniqe words: " + task4Obj.numberOfUniqeWords());
    System.out.println("List of uniqe words: " + task4Obj.listOfUniqueWords());
    task4Obj.wordOccurence();
    task4Obj.symbolOccurence();
  }
}
