package com.pikulyk.task1;

public class MyLambdaTask1 {

  public static void main(String[] args) {

    Countable maxVal = (a, b, c) -> {
      if (a > b && a > c) {
        return a;
      } else if (b > a && b > c) {
        return b;
      }
      return c;
    };

    Countable averVal = (a, b, c) -> (a + b + c) / 3;

    System.out.println("Max val: " + maxVal.count(3, 8, 5));
    System.out.println("Average val: " + averVal.count(1, 6, 7));
  }
}
