package com.pikulyk.task1;

@FunctionalInterface
public interface Countable {
  int count(int a, int b, int c);
}
