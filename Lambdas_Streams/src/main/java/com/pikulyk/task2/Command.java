package com.pikulyk.task2;

@FunctionalInterface
public interface Command {
  void execute(String var);
}
