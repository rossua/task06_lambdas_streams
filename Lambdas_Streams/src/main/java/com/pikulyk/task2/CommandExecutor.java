package com.pikulyk.task2;

import java.util.HashMap;
import java.util.Map;

public class CommandExecutor {

  private Map<String, Command> command;

  public CommandExecutor() {
    this.command = commandMapExecutor();
  }

  public Map<String, Command> getCommand() {
    return command;
  }

  private Map<String, Command> commandMapExecutor() {
    Map<String, Command> command = new HashMap<>();
    CommandClass cmdObject = new CommandClass();

    command.put("1", (arg -> System.out.println("Executed with Lambda: " + arg + "\n")));
    command.put("2", CommandExecutor::methodRef);
    command.put("3", cmdObject);
    command.put("4", new Command() {
      @Override
      public void execute(String arg) {
        System.out.println("Executed with anonymous class: " + arg + "\n");
      }
    });
    return command;
  }

  public static void methodRef(String arg) {
    System.out.println("Executed with method reference: " + arg + "\n");
  }

  public void showMenu() {
    System.out.println("1) As lambda function\n"
        + "2) As method reference\n"
        + "3) As object of class\n"
        + "4) As anonymous class\n"
        + "Q - exit\n");
  }
}
