package com.pikulyk.task2;

public class CommandClass implements Command{
  @Override
  public void execute(String arg) {
    System.out.println("Executed with class: " + arg + "\n");
  }

}
